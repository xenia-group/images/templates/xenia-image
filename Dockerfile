FROM registry.hub.docker.com/gentoo/portage:latest AS portage
FROM registry.hub.docker.com/gentoo/stage3:amd64-desktop-systemd-mergedusr
COPY --from=portage /var/db/repos/gentoo /var/db/repos/gentoo


RUN mkdir /tmp/img
WORKDIR /tmp/img
COPY ./ ./

RUN rm /etc/portage/make.profile
RUN cd /etc/portage && ln -sf ../../var/db/repos/gentoo/profiles/$([ -e "/tmp/img/profile" ] && cat /tmp/img/profile || cat /tmp/img/global/profile) /etc/portage/make.profile

RUN shopt -s dotglob && cp -r /tmp/img/config/config/* /etc/portage/
RUN shopt -s dotglob && cp -r /tmp/img/overlay/overlay/* /

RUN shopt -s dotglob && cp -r /tmp/img/config/local/* /etc/portage/
RUN shopt -s dotglob && cp -r /tmp/img/overlay/local/* /

RUN cat global/make.conf.append >> /etc/portage/make.conf

RUN emerge --quiet --getbinpkg dev-vcs/git

RUN emaint sync -a

RUN emerge --quiet --getbinpkg --changed-use --update --deep $(cat global/package.list) && emerge --getbinpkg --changed-use --update --deep $(cat package.list)

RUN chmod +x global/fsscript.sh && chmod +x fsscript.sh && global/fsscript.sh && ./fsscript.sh

