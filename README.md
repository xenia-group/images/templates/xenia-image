# Xenia base image

This is the base OCI image template that others are created from.

## Overview

**OS:** Gentoo Linux

**DE:** N/A

**Init system:** N/A

## Details

`./config` - Portage config (`/etc/portage`)

- `config/local` - Added by the specific image
- `config/config` - [Global config on all images](https://gitlab.com/xenia-group/images/common/config)

`./overlay` - Filesystem overlay

- `overlay/local` - Overlay for the specific image
- `overlay/overlay` - [Global overlay on all images](https://gitlab.com/xenia-group/images/common/overlay)

`./global` - [Global config](https://gitlab.com/xenia-group/images/common/global)

- `fsscript.sh` - Script that runs at end of image creation
- `make.conf.append` - Options added to make.conf
- `profile` - Gentoo profile (default, overwritten by local)
- `package.list` - Default package list

`./` - Local settings

- `fsscript.sh` - Script that runs at end of image creation, after global fsscript
- `package.list` - Additional packages for the specific image

